#pragma once

#include <iostream>
#include <string>

using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string name, int baseHp, int hp, int level, int baseDamage, int exp, int expToNextLevel);
	void displayStats();
	void healPokemon();
	bool pokemonIsAlive();
	bool pokemonIsDead();
	void attack(Pokemon* enemy);
	void pokemonGainExp();
	void pokemonLevelUp();
private:
	string name;
	int baseHp;
	int hp;
	int level;
	int baseDamage;
	int exp;	
	int expToNextLevel;
};