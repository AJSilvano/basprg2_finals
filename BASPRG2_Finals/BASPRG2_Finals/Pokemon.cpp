#include "Pokemon.h"

Pokemon::Pokemon()
{
	this->name = "";
	this->baseHp = 0;
	this->hp = 0;
	this->level = 0;
	this->baseDamage = 0;
	this->exp = 0;
	this->expToNextLevel = 0;
}

Pokemon::Pokemon(string name, int baseHp, int hp, int level, int baseDamage, int exp, int expToNextLevel)
{
	this->name = name;
	this->baseHp = baseHp;
	this->hp = hp;
	this->level = level;
	this->baseDamage = baseDamage;
	this->exp = exp;
	this->expToNextLevel = expToNextLevel;
}

void Pokemon::displayStats()
{
	cout << "[" << this->name << " Stats]" << endl;
	cout << endl;
	cout << "Health: " << this->hp << endl;
	cout << "Pokemon Level: " << this->level << endl;
	cout << "Damage: " << this->baseDamage << endl;
	cout << "Exp: " << this->exp << endl;
}

void Pokemon::healPokemon()
{
	this->hp = this->baseHp;
}

bool Pokemon::pokemonIsAlive()
{
	return hp > 0;
}

bool Pokemon::pokemonIsDead()
{
	return hp <= 0;
}

void Pokemon::attack(Pokemon* enemy)
{
	int pokemonAttackChance = rand() % 100 + 1;

	if (pokemonAttackChance <= 80)
	{
		cout << this->name << " attacked " << enemy->name << "!" << endl;
		cout << enemy->name << " took " << this->baseDamage << " damage!" << endl;
		enemy->hp -= this->baseDamage;
		cout << enemy->name << " has " << enemy->hp << " hp left." << endl;
		system("pause");
	}

	else if (pokemonAttackChance > 80)
	{
		cout << this->name << " attacked " << enemy->name << "!" << endl;
		cout << this->name << "'s" << " attacked missed!" << endl;
		system("pause");
	}
}

void Pokemon::pokemonGainExp()
{
	this->exp += (this->expToNextLevel * 0.20);
}

void Pokemon::pokemonLevelUp()
{
	if (this->exp >= this->expToNextLevel)
	{
		this->level++;
		cout << this->name << " Leveled up to " << this->level << "!" << endl;
		this->baseHp += (this->baseHp * 0.15);
		this->hp += (this->hp * 0.15);
		this->exp = 0;
		this->expToNextLevel += (this->expToNextLevel * 0.20);
		system("pause");
	}
}
